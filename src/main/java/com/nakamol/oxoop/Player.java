package com.nakamol.oxoop;

import java.io.Serializable;

/**
 *
 * @author OS
 */
public class Player implements Serializable{

    private char symbol;
    private int win;
    private int loss;
    private int draw;

    public Player(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }

    public int getWin() {
        return win;
    }

    public void Win() {
        this.win++;
    }

    public int getLoss() {
        return loss;
    }

    public void Loss() {
        this.loss++;
    }

    public int getDraw() {
        return draw;
    }

    public void Draw() {
        this.draw++;
    }

    @Override
    public String toString() {
        return "Player " + symbol + " win: " + win + " loss: " + loss + " draw: " + draw;
    }
}
